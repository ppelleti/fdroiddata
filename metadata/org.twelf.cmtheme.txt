Categories:System
License:GPLv3
Web Site:https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/README.md
Source Code:https://gitlab.com/xphnx/twelf_cm12_theme
Issue Tracker:https://gitlab.com/xphnx/twelf_cm12_theme/issues
Changelog:https://gitlab.com/xphnx/twelf_cm12_theme/blob/HEAD/CHANGELOG.md

Auto Name:TwelF
Summary:CM12 FLOSS Theme
Description:
TwelF is a Material Design inspired theme for Android Lollipop aiming to
provide a consistent and minimalistic look to your device. Your ROM must
include the CM12 Theme Engine.

Features:

* FLOSS Icon Pack
* Bootanimation
* Wallpaper & Lockscreen
* Alarm & Ringtone

For issues, comments and icon request, please use the issue tracker.
.

Repo Type:git
Repo:https://gitlab.com/xphnx/twelf_cm12_theme.git

Build:0.1,1
    disable=remove apk
    commit=v0.1
    subdir=theme
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=theme
    gradle=yes

Build:0.3,3
    commit=v0.3
    subdir=theme
    gradle=yes

Build:0.4,4
    commit=v0.4
    subdir=theme
    gradle=yes

Build:0.7,7
    commit=v0.7
    subdir=theme
    gradle=yes

Build:0.8,8
    commit=1903b59c91a73a38bcb5591325c4efbd71a4235d
    subdir=theme
    gradle=yes

Build:1.0,10
    commit=v1.0
    subdir=theme
    gradle=yes

Build:1.1,11
    commit=v1.1
    subdir=theme
    gradle=yes

Build:1.2,12
    commit=v1.2
    subdir=theme
    gradle=yes

Build:1.3,13
    disable=remove apk
    commit=v1.3
    subdir=theme
    gradle=yes

Build:1.4,14
    commit=v1.4
    subdir=theme
    gradle=yes

Build:2.0,20
    commit=v2.0
    subdir=theme
    gradle=yes

Build:2.2,22
    commit=v2.2
    subdir=theme
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.2
Current Version Code:22

