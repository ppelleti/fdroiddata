Categories:System
License:GPLv3
Web Site:https://github.com/markmcavoy/androidtoken/blob/master/README.md
Source Code:https://github.com/markmcavoy/androidtoken
Issue Tracker:https://github.com/markmcavoy/androidtoken/issues

Auto Name:Android Token
Summary:OATH software tokens
Description:
Turning a mobile phone into a One Time Password (OTP) generation device which
can be used in the place of hardware tokens.

Support for provisioning tokens using the KeyUriFormat and QR codes as well
as manual creation.

Can optionally be protected with a PIN to stop unauthorised access to the
software tokens.

Supports both HOTP (Event Tokens) and TOTP (Time Tokens)
specifications.
.

Repo Type:git
Repo:https://github.com/markmcavoy/androidtoken.git

Build:2.02,4
    commit=b35d1f5
    subdir=src
    target=android-16

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.02
Current Version Code:4

