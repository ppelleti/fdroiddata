Categories:System
License:AGPLv3
Web Site:https://github.com/Freeyourgadget/Gadgetbridge/blob/HEAD/README.md
Source Code:https://github.com/Freeyourgadget/Gadgetbridge
Issue Tracker:https://github.com/Freeyourgadget/Gadgetbridge/issues
Changelog:https://github.com/Freeyourgadget/Gadgetbridge/blob/master/CHANGELOG.md

Auto Name:Gadgetbridge
Summary:Communicate with your Pebble/Miband
Description:
Use your Pebble/Miband without the vendor's closed source application and
without the need to create an account and transmit any of your data to the
vendor's servers.

Features (Pebble):
* Incoming calls notification and display (caller, phone number)
* Outgoing call display
* Reject/hangup calls
* SMS notification (sender, body)
* K-9 Mail notification support (sender, subject, preview)
* Support for generic notificaions (above filtered out)
* Apollo music playback info (artist, album, track)
* Music control: play/pause, next/previous track, volume up/down
* List and remove installed apps/watchface
* Install .pbw files
* install firmware from .pbz files (EXPERIMENTAL)

For more information please read the [https://github.com/Freeyourgadget/Gadgetbridge/blob/master/README.md README]
.

Repo Type:git
Repo:https://github.com/Freeyourgadget/Gadgetbridge.git

Build:0.1.0,1
    commit=0.1.0
    subdir=app
    gradle=yes

Build:0.1.1,2
    commit=0.1.1
    subdir=app
    gradle=yes

Build:0.1.2,3
    commit=0.1.2
    subdir=app
    gradle=yes

Build:0.1.3,4
    commit=0.1.3
    subdir=app
    gradle=yes

Build:0.1.4,5
    commit=0.1.4
    subdir=app
    gradle=yes

Build:0.1.5,6
    commit=0.1.5
    subdir=app
    gradle=yes

Build:0.2.0,7
    commit=0.2.0
    subdir=app
    gradle=yes

Build:0.3.1,9
    commit=0.3.1
    subdir=app
    gradle=yes

Build:0.3.2,10
    commit=0.3.2
    subdir=app
    gradle=yes

Build:0.3.3,11
    commit=0.3.3
    subdir=app
    gradle=yes

Build:0.3.4,12
    commit=0.3.4
    subdir=app
    gradle=yes

Build:0.3.5,13
    commit=0.3.5
    subdir=app
    gradle=yes

Build:0.4.0,14
    commit=0.4.0
    subdir=app
    gradle=yes

Build:0.4.1,15
    commit=0.4.1
    subdir=app
    gradle=yes

Build:0.4.2,16
    commit=0.4.2
    subdir=app
    gradle=yes

Build:0.4.3,17
    commit=0.4.3
    subdir=app
    gradle=yes

Build:0.4.4,18
    commit=0.4.4
    subdir=app
    gradle=yes

Build:0.4.5,19
    commit=0.4.5
    subdir=app
    gradle=yes

Build:0.4.6,20
    commit=0.4.6
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.4.6
Current Version Code:20

