Categories:Games
License:MIT
Web Site:https://github.com/AEFeinstein/mtg-familiar/blob/HEAD/README.md
Source Code:https://github.com/AEFeinstein/mtg-familiar
Issue Tracker:https://github.com/AEFeinstein/mtg-familiar/issues
Donate:https://github.com/AEFeinstein/mtg-familiar/blob/HEAD/README.md#show-support

Auto Name:MTG Familiar
Summary:Utilities for the game MTG
Description:
This application provides a way to search an offline database of all magic
cards. The application also provides a life counter, a random number
generator, card price lookups, and card image downloads. Future features
include deck management, rules, and anything else which would be useful while
playing Magic.
.

Repo Type:git
Repo:https://github.com/AEFeinstein/mtg-familiar

#old repo, gplv3
Build:1.8.1,15
    commit=474
    subdir=MTGFamiliar
    update=.,../actionbarsherlock

#old repo, gplv3
Build:2.0.4,20
    commit=Version 2.0.4
    subdir=MTGFamiliar
    prebuild=sed -i '/guava/d' project.properties
    update=.,../SlidingMenu,../actionbarsherlock,../io

#old repo, gplv3
Build:2.1.1,22
    commit=Version 2.1.1
    subdir=MTGFamiliar
    prebuild=cp ../actionbarsherlock/libs/android-support-v4.jar ../robospice/libs/

Build:3.2.6,35
    disable=play-services
    commit=33481bbcdd29f92018f130b1c38b0f1e05051448
    subdir=mobile
    gradle=yes
    rm=wear
    prebuild=sed -i -e '/play-services/d' -e '/wear/d' build.gradle && \
        echo "include ':mobile',  ':FamiliarConstants'" > ../settings.gradle && \
        pushd ../ && \
        mv familiarconstants FamiliarConstants && \
        popd

Maintainer Notes:
See https://github.com/AEFeinstein/mtg-familiar/issues/106
License switched when moving from github.
.

Archive Policy:0 versions
Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.2.6
Current Version Code:35

