AntiFeatures:NonFreeNet
Categories:Internet
License:GPLv2+
Web Site:https://github.com/indywidualny/FacebookLite/blob/HEAD/README.md
Source Code:https://github.com/indywidualny/FacebookLite
Issue Tracker:https://github.com/indywidualny/FacebookLite/issues
Changelog:https://github.com/indywidualny/FacebookLite/releases
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=koras%2eevil%40gmail%2ecom&lc=GB&item_name=Krzysztof%20Grabowski&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted
Bitcoin:1JUaZytkub2CP5jhRYQDDY6pibqrUUSp2y

Auto Name:Facebook Lite
Summary:Connect to Facebook
Description:
Unofficial app built around the mobile Facebook site.
.

Repo Type:git
Repo:https://github.com/indywidualny/FacebookLite

Build:1.3.2,1
    commit=v1.3.2
    subdir=app
    gradle=yes

Build:1.4.0,2
    commit=v1.4.0
    subdir=app
    gradle=yes

Build:1.4.1,3
    commit=v1.4.1
    subdir=app
    gradle=yes

Build:1.5.0,4
    commit=v1.5.0
    subdir=app
    gradle=yes

Build:1.5.2,6
    commit=v1.5.2
    subdir=app
    gradle=yes

Build:1.6.0,7
    disable=play-services
    commit=v1.6.0
    subdir=app
    gradle=yes

Build:1.6.1,8
    commit=v1.6.1
    subdir=app
    gradle=yes

Build:1.6.2,9
    commit=v1.6.2
    subdir=app
    gradle=yes

Build:1.7.0,10
    commit=v1.7.0
    subdir=app
    gradle=yes

Build:1.7.1,11
    disable=request by upstream
    commit=v1.7.1
    subdir=app
    gradle=yes

Build:1.7.2,12
    commit=v1.7.2
    subdir=app
    gradle=yes

Build:1.7.3,13
    commit=v1.7.3
    subdir=app
    gradle=yes

Build:1.7.4,14
    commit=v1.7.4
    subdir=app
    gradle=yes

Build:1.7.5,15
    commit=v1.7.5
    subdir=app
    gradle=yes

Build:1.7.6,16
    commit=v1.7.6
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.7.6
Current Version Code:16

