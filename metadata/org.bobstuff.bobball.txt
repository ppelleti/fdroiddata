Categories:Games
License:FreeBSD
Web Site:https://github.com/bobthekingofegypt/BobBall/blob/HEAD/README.mkd
Source Code:https://github.com/bobthekingofegypt
Issue Tracker:https://github.com/bobthekingofegypt/BobBall/issues

Auto Name:BobBall
Summary:Jezzball style clone
Description:
BobBall is a simple version of the old game Jezzball.

Features:
* Easy to play, hard to master
* Infinite levels
* Top scores table
.

Repo Type:git
Repo:https://github.com/bobthekingofegypt/BobBall.git

Build:1.0,1
    commit=v1.0

Build:1.3.1,5
    commit=v1.3.1

Build:1.4,6
    commit=v1.4

Build:1.5,7
    commit=v1.5

Build:1.5.1,8
    commit=v1.5.1

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.5.1
Current Version Code:8

