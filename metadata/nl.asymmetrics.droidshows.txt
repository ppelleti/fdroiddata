Categories:Multimedia,Internet
License:GPLv3
Web Site:http://ltguillaume.github.io/DroidShows
Source Code:https://github.com/ltGuillaume/DroidShows
Issue Tracker:https://github.com/ltGuillaume/DroidShows/issues

Auto Name:DroidShows
Summary:TV series browser and tracker
Description:
TV Series/TV Shows browser and tracker based on [[org.droidseries]]. It
helps you manage your favorite TV shows and keep track of the episodes
that you have or have not seen.
.

Repo Type:git
Repo:https://github.com/ltGuillaume/DroidShows

Build:4.2,13
    commit=cd464a47be4cbcda2a50cd1bdebea586d87e671e

Build:4.3,43
    commit=d0a8de63d8f25880ef5be5e471d9daaca196fd68

Build:4.4,44
    commit=4ccc631d7cabf5376162d7993ebbf52c36ee9f93

Build:4.5,45
    commit=4.5

Build:4.6,46
    commit=4.6

Build:4.7,47
    commit=4.7

Build:4.8,48
    commit=4.8

Build:4.9,49
    commit=4.9

Build:5.0,50
    commit=5.0

Build:5.1,51
    commit=5.1

Build:5.2,52
    commit=5.2

Build:5.3,53
    commit=5.3

Build:5.4,54
    commit=5.4

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:5.4
Current Version Code:54

